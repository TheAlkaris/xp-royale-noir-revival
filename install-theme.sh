#!/bin/bash

## XP Royale Noir Revival theme
## inspired by Windows XP themes
## but made for modern Linux desktops.
##
## - Alkaris ©2024

## Theme install script that has a couple functions
## to install themes, with the option to install ALL
##  or 1 or 3 different kind of themes from the selection.


# Define color codes
CLR="\033[0m"
COLOR_GREEN="\033[38;2;57;234;73m"
COLOR_RED="\033[38;2;242;80;86m"
COLOR_BLUE="\033[38;2;82;148;226m"
COLOR_YELLOW="\033[38;2;250;197;54m"

# Theme list
## There are 7 original classic themes and 7 dark version themes
THEMES=(
  "XP Royale"
  "XP Silver"
  "XP Olive"
  "XP Royale Blue"
  "XP Royale Noir"
  "XP Royale Zune"
  "XP Royale Vista"
  "XP Royale Dark"
  "XP Silver Dark"
  "XP Olive Dark"
  "XP Royale Blue Dark"
  "XP Royale Noir Dark"
  "XP Royale Zune Dark"
  "XP Royale Vista Dark"
)

# Function to list themes, in total there are 14 themes
list_themes() {
  echo -e "${COLOR_YELLOW}Available themes:${CLR}"
  for i in "${!THEMES[@]}"; do
    echo "$((i+1)). ${THEMES[$i]}"
  done
}

# Function to install a theme
## Theme source path is: themes/
##  install_theme checks source directory exists
##  before copying file contents to destination.
install_theme() {
  theme_name=$1
  install_path=$2

  theme_source="themes/$theme_name"

  if [ ! -d "$theme_source" ]; then
    echo -e "${COLOR_RED}Theme source directory '$theme_source' does not exist.${CLR}"
    exit 1
  fi

  echo "Installing theme '$theme_name' to '$install_path'"
  mkdir -p "$install_path"
  cp -r "$theme_source"/* "$install_path/"
}

# Function to install multiple themes
## We can install multiple themes at once by
##  specifying the numbers from the list with
##  a spacing between them, ie; `1 4 12`
install_selected_themes() {
  install_path=$1
  shift
  theme_numbers=("$@")

  for num in "${theme_numbers[@]}"; do
    theme_index=$((num-1))
    if [ "$theme_index" -ge 0 ] && [ "$theme_index" -lt "${#THEMES[@]}" ]; then
      install_theme "${THEMES[$theme_index]}" "$install_path"
    else
      echo "Invalid theme number: $num"
    fi
  done
}

# Function to install all themes
install_all_themes() {
  install_path=$1

  for theme in "${THEMES[@]}"; do
    install_theme "$theme" "$install_path"
  done
}

# Function to remove all themes
remove_theme() {
  theme_name=$1
  install_path=$2

  theme_target="$install_path/$theme_name"

  if [ ! -d "$theme_target"]; then
     echo -e "${COLOR_RED}Theme source directory '${COLOR_GREEN}$theme_source${CLR}${COLOR_RED}' does not exist.${CLR}"
     exit 1
  fi

  echo "Removing '${COLOR_GREEN}$theme_name${CLR}' from '${COLOR_GREEN}$install_path${CLR}'"
  rm -rf "$theme_target"
}

## Function to show help
## Options:
##   -l        list themes
##   -s num    select theme with numbers
##   -a        system-wide install
##   -u        userspace install
##   -r        remove installed themes by cleaning up all files
##   -h        to show help menu duh! :P
show_help() {
  echo -e " "
  echo -e "Usage: $0 [${COLOR_YELLOW}-l${CLR}] [${COLOR_YELLOW}-s${CLR} num] [${COLOR_YELLOW}-a${CLR}] [${COLOR_YELLOW}-u${CLR}] [${COLOR_YELLOW}-h${CLR}]"
  echo
  echo -e "${COLOR_YELLOW}Options:${CLR}"
  echo -e "  ${COLOR_YELLOW}-l${CLR}              Show list of available themes"
  echo -e "  ${COLOR_YELLOW}-s${CLR} num          Select themes to install by their numbers, e.g., ${COLOR_YELLOW}-s${CLR} ${COLOR_BLUE}1 4 6${CLR}"
  echo -e "  ${COLOR_YELLOW}-a${CLR}              Install all themes to ${COLOR_GREEN}/usr/share/themes${CLR} (requires ${COLOR_RED}sudo${CLR})"
  echo -e "  ${COLOR_YELLOW}-u${CLR}              Install themes to ${COLOR_GREEN}/home/$USER/.themes${CLR}"
  echo -e "  ${COLOR_YELLOW}-r${CLR}              Remove installed themes"
  echo -e "  ${COLOR_YELLOW}-h${CLR}              Show this help message"
  echo
  echo -e "${COLOR_YELLOW}Examples:${CLR}"
  echo -e "  $0 ${COLOR_YELLOW}-l${CLR}"
  echo -e "    List all available themes"
  echo
  echo -e "  $0 ${COLOR_YELLOW}-s${CLR} ${COLOR_BLUE}1 4 6${CLR}"
  echo -e "    Install themes 1, 4, and 6 to ${COLOR_GREEN}/usr/share/themes${CLR} (requires ${COLOR_RED}sudo${CLR})"
  echo
  echo -e "  $0 ${COLOR_YELLOW}-a${CLR}"
  echo -e "    Install all themes to ${COLOR_GREEN}/usr/share/themes${CLR} (requires ${COLOR_RED}sudo${CLR})"
  echo
  echo -e "  $0 ${COLOR_YELLOW}-u${CLR} ${COLOR_YELLOW}-s${CLR} ${COLOR_BLUE}1 3${CLR}"
  echo -e "    Install themes 1 and 3 to ${COLOR_GREEN}/home/$USER/.themes${CLR}"
  echo -e " "
}

# Parse flags
## Whenever you see $USER it will always resolve to your current login user name
# Your install_path becomes whatever your chosen selection of install.
install_path=""
selected_themes=()
install_all=false

# certain flag options can be combined.
# mainly, -u and -s flags.
while getopts "ls:auh" flag; do
  case "${flag}" in
    l)
      list_themes
      exit 0
      ;;
    s)
      selected_themes=(${OPTARG})
      ;;
    a)
      install_all=true
      ;;
    u)
    # If your default path to your .themes directory is different, change it here,
    #  otherwise install_all to /usr/share/themes with `sudo`` and `-a` flag.
      install_path="/home/$USER/.themes"
      ;;
    r)
      remove_themes=(${OPTARG})
      ;;
    h)
      show_help
      exit 0
      ;;
    *)
      show_help
      exit 1
      ;;
  esac
done

# Tell the user how to get started when run without flags.
if [ -z "$install_path" ]; then
  if [ "$(id -u)" -ne 0 ]; then
    echo -e " "
    echo -e "To install themes globally to ${COLOR_GREEN}/usr/share/themes${CLR}, please run with ${COLOR_RED}sudo${CLR}."
    echo -e " "
    if [ -z "$install_path" ]; then
      echo -e "Please pass ${COLOR_YELLOW}-u${CLR} and ${COLOR_YELLOW}-s${CLR} flags to install themes for user: ${COLOR_BLUE}$USER${CLR}."
      echo -e "For help see: ${COLOR_YELLOW}-h${CLR} flag."
      echo -e " "
      exit 1
    fi
  else
    install_path="/usr/share/themes"
  fi
fi

# Install themes from user selected inputs.
# if you haven't specified flag `-s num` it'll warn the user.
# unless you're using the `-a` flag which installs ALL themes with; `sudo ./install-theme.sh -a`
if $install_all; then
  install_all_themes "$install_path"
elif [ "${selected_themes[@]}" -ne 0 ]; then
  install_selected_themes "$install_path" "${selected_themes[@]}"
else
  echo -e "\nNo theme specified. \n\tUse ${COLOR_YELLOW}-l${CLR} flag to list available themes.\n"
  exit 1
fi

# Removing ALL themes
if $remove_all; then
   remove_themes "$install_path"
   echo -e "XP Royale Noir Revival themes from $install_path deleted!"
   exit 0
elif
   echo -e "${COLOR_RED}Themes in '${COLOR_GREEN}$install_path${CLR}${COLOR_RED}' does not exist.${CLR}"
   exit 1

# Removing themes based on selections, `sudo ./install-theme.sh -r 1 4` removes themes 1 and 4
if [ "${remove_themes[@]}" -ne 0 ]; then
   remove_selected_themes "install_path" "${remove_themes[@]}"
   exit 0

elif
   echo -e "${COLOR_RED}Themes in '${COLOR_GREEN}$install_path${CLR}${COLOR_RED}' does not exist.${CLR}"
   exit 1
fi