# XP Royale Noir Revival
This project aims to recreate one of the most iconic desktop themes of the early 2000s with Windows XP's Royale Noir and Zune themes for Linux Desktops. Not only do I want to recreate these themes but to bring it into modern day desktop themes with options for classic Light theme as well as Dark theme as well.

#### Recreate Theme Goal:
![alt text](Windows-XP-themes-microsoft-windows-37521593-800-238.png "XP Themes")

- [ ] XP Royale
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme
- [ ] XP Silver
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme
- [ ] XP Olive
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme
- [ ] XP Royale Blue
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme
- [ ] XP Royale Noir
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme
- [ ] XP Royale Zune
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme
- [ ] XP Royale Vista
  - [ ] GNOME/GTK Theme
  - [ ] KDE Theme


I want to make this work with just about any desktop environment with GNOME, KDE, GTK etc... to first create original 1:1 look and feel and then a more compact cleaner modern version, and create Dark theme of each style.

[def xp-themes-sample "XP Themes"]: Windows-XP-themes-microsoft-windows-37521593-800-238.png

---

### [Contributing](CONTRIBUTING.md)
### [GNU GPL v3.0 License](LICENSE)
### [Code of Conduct](CODE_OF_CONDUCT.md)